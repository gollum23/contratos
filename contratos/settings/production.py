# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '27/12/14'
from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS += (
    'gunicorn',
)

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['DB_CONTRATOS'],
        'USER': os.environ['DB_USER'],
        'PASSWORD': os.environ['DB_PASS']
    }
}