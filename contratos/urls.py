from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'contratos.views.home', name='home'),
    url(r'^', include('home.urls', namespace='home')),
    url(r'^contratos/', include('contract.urls', namespace='contract')),
    url(r'^contratistas/', include('contractor.urls', namespace='contractor')),
    url(r'^pagos/', include('payment.urls', namespace='payment')),

    url(r'^admin/', include(admin.site.urls)),
)
