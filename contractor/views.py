# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'gollum23'
__date__ = '3/7/15'

from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, FormView
from django.views.generic.edit import CreateView, UpdateView

from .forms import ContractorForm, FilterContractForm
from .models import Contractor
from home.mixins import LoginRequiredMixin


class HomeContractorView(LoginRequiredMixin, FormView, ListView):
    model = Contractor
    template_name = 'contractor__index.html'
    form_class = FilterContractForm
    data = {}

    def get_queryset(self):
        self.data = {
            'name': '',
            'identification': ''
        }
        queryset = self.model.objects.all().order_by('name')
        if self.request.GET.get('name') is not None and self.request.GET.get('name') != '':
            by_name = self.request.GET.get('name')
            self.data['name'] = by_name
            queryset = queryset.filter(name__icontains=by_name)
        if self.request.GET.get('identification') is not None and self.request.GET.get('identification') != '':
            by_id = self.request.GET.get('identification')
            self.data['identification'] = by_id
            queryset = queryset.filter(identification__icontains=by_id)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(HomeContractorView, self).get_context_data(**kwargs)
        context['form'] = self.form_class(initial=self.data)
        return context


class AddContractorView(LoginRequiredMixin, CreateView):
    form_class = ContractorForm
    template_name = 'contractor__add.html'
    success_url = reverse_lazy('contractor:home')


class EditContractorView(LoginRequiredMixin, UpdateView):
    model = Contractor
    form_class = ContractorForm
    template_name = 'contractor__edit.html'
    success_url = reverse_lazy('contractor:home')


class DetailContractorView(LoginRequiredMixin, DetailView):
    model = Contractor
    template_name = 'contractor__detail.html'
