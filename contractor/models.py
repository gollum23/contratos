# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'gollum23'
__date__ = '3/7/15'

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from home.models import TimestampUser


@python_2_unicode_compatible
class IdentificationType(models.Model):
    name_idtype = models.CharField(
        verbose_name='Tipo de identificación',
        max_length=20
    )

    class Meta:
        verbose_name = 'Tipo de identificación'
        verbose_name_plural = 'Tipos de identificación'

    def __str__(self):
        return '%s' % self.name_idtype


@python_2_unicode_compatible
class Contractor(TimestampUser):
    identification_type = models.ForeignKey(
        IdentificationType,
        verbose_name='Tipo de identificación'
    )
    identification = models.PositiveIntegerField(
        verbose_name='Identificación'
    )
    dv = models.PositiveSmallIntegerField(
        validators=[
            MinValueValidator(0),
            MaxValueValidator(9)
        ],
        blank=True,
        null=True
    )
    name = models.CharField(
        verbose_name='Nombre',
        max_length=150
    )
    address = models.CharField(
        verbose_name='Dirección',
        max_length=255
    )
    phone = models.PositiveIntegerField(
        verbose_name='Teléfono'
    )
    cellphone = models.BigIntegerField(
        verbose_name='Celular',
        blank=True
    )
    agent = models.CharField(
        verbose_name='Representante Legal',
        max_length=150,
        blank=True
    )

    class Meta:
        verbose_name = 'Contratista'
        verbose_name_plural = 'Contratistas'

    def __str__(self):
        return '%s' % self.name