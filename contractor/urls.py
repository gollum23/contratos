# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '3/8/15'

from django.conf.urls import patterns, url

from .views import AddContractorView, HomeContractorView, EditContractorView, DetailContractorView

urlpatterns = patterns(
    'contractor.views',
    url(r'^$', HomeContractorView.as_view(), name='home'),
    url(r'^add/$', AddContractorView.as_view(), name='add'),
    url(r'^edit/(?P<pk>\d+)/$', EditContractorView.as_view(), name='edit'),
    url(r'^detail/(?P<pk>\d+)/$', DetailContractorView.as_view(), name='detail')
)