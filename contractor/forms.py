# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'gollum23'
__date__ = '3/11/15'

from django import forms

from .models import Contractor


class ContractorForm(forms.ModelForm):
    identification = forms.IntegerField(
        widget=forms.TextInput(
            attrs={
                'class': 'input--auto-width',
                'placeholder': 'Número'
            }
        )
    )
    dv = forms.IntegerField(
        widget=forms.TextInput(
            attrs={'class': 'input--d2'}
        ),
        required=False
    )
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nombre', 'class': 'full-width'}
        )
    )
    address = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Dirección', 'class': 'full-width'}
        )
    )
    phone = forms.IntegerField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Teléfono', 'class': 'full-width'}
        )
    )
    cellphone = forms.IntegerField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Celular', 'class': 'full-width'}
        ),
        required=False
    )
    agent = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Representate legal', 'class': 'full-width'}
        ),
        required=False
    )

    class Meta:
        model = Contractor
        fields = '__all__'


class FilterContractForm(forms.Form):
    name = forms.CharField(
        label='Nombre',
        max_length=255
    )
    identification = forms.IntegerField(
        widget=forms.TextInput(),
        label='Identificación'
    )
