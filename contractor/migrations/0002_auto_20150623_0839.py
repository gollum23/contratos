# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contractor', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contractor',
            name='cellphone',
            field=models.BigIntegerField(verbose_name='Celular', blank=True),
        ),
    ]
