# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contractor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('identification', models.PositiveIntegerField(verbose_name='Identificaci\xf3n')),
                ('dv', models.PositiveSmallIntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(9)])),
                ('name', models.CharField(max_length=150, verbose_name='Nombre')),
                ('address', models.CharField(max_length=255, verbose_name='Direcci\xf3n')),
                ('phone', models.PositiveIntegerField(verbose_name='Tel\xe9fono')),
                ('cellphone', models.BigIntegerField(verbose_name='Celular')),
                ('agent', models.CharField(max_length=150, verbose_name='Representante Legal', blank=True)),
            ],
            options={
                'verbose_name': 'Contratista',
                'verbose_name_plural': 'Contratistas',
            },
        ),
        migrations.CreateModel(
            name='IdentificationType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_idtype', models.CharField(max_length=20, verbose_name='Tipo de identificaci\xf3n')),
            ],
            options={
                'verbose_name': 'Tipo de identificaci\xf3n',
                'verbose_name_plural': 'Tipos de identificaci\xf3n',
            },
        ),
        migrations.AddField(
            model_name='contractor',
            name='identification_type',
            field=models.ForeignKey(verbose_name='Tipo de identificaci\xf3n', to='contractor.IdentificationType'),
        ),
        migrations.AddField(
            model_name='contractor',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
