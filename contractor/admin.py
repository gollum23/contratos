# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '3/7/15'

from django.contrib import admin

from .models import IdentificationType


@admin.register(IdentificationType)
class IdentificationTypeAdmin(admin.ModelAdmin):
    pass
