/**
 * Created by gollum23 on 19/06/15.
 */

(function() {
    'use strict';
    var $ = window.jQuery;

    $(window ).on('beforeunload', function() {
        return "Los cambios no guardados se perderan.";
    });

    $('#formContractor').on('submit', function() {
        $(window ).off('beforeunload');
    })

})();
