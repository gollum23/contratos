/**
 * Created by gollum23 on 20/06/15.
 */
(function() {
    $('#reset_filter_form').on('click', resetFilterForm);

    function resetFilterForm(e) {
        e.preventDefault();
        $ ('.form__filter' ).find('input' ).val('');
        window.location.replace('/contratistas/');
    }
})();
