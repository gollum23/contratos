# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'gollum23'
__date__ = '3/7/15'
from datetime import datetime
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from contractor.models import Contractor
from home.models import TimestampUser, Typology, Project, ResponsiveArea


def contract_folder(self, name):
    return 'contracts/' + name


def extension_folder(self, name):
    return 'extensions/' + name


def otheryes_folder(self, name):
    return 'otheryes/' + name


def addition_folder(self, name):
    return 'additions/' + name


STATES_CONTRACT = (
    ('celebrado', 'Celebrado'),
    ('ejecucion', 'En ejecución'),
    ('terminado', 'Terminado'),
)


@python_2_unicode_compatible
class ContractState(models.Model):
    name_state = models.CharField(
        verbose_name='Estado de Contrato',
        max_length=100
    )

    class Meta:
        verbose_name = 'Estado de contratos'
        verbose_name_plural = 'Estados de contratos'

    def __str__(self):
        return '%s' % self.name_state


@python_2_unicode_compatible
class Contract(TimestampUser):
    contract_number = models.PositiveIntegerField(
        verbose_name='Número de contrato',
    )
    contract_year = models.PositiveIntegerField(
        default=datetime.now().year
    )
    contractor = models.ForeignKey(
        Contractor,
        verbose_name='Contratista'
    )
    typology = models.ForeignKey(
        Typology,
        verbose_name='Tipología'
    )
    contract_object = models.TextField(
        verbose_name='Objeto del contrato'
    )
    subscription_date = models.DateField(
        verbose_name='Fecha de suscripción'
    )
    init_date = models.DateField(
        verbose_name='Fecha de inicio'
    )
    end_date = models.DateField(
        verbose_name='Fecha de terminación'
    )
    project = models.ForeignKey(
        Project,
        verbose_name='Proyecto'
    )
    responsive_area = models.ForeignKey(
        ResponsiveArea,
        verbose_name='Área responsable'
    )
    total_amount = models.PositiveIntegerField(
        verbose_name='Valor total'
    )
    advance_amount = models.PositiveIntegerField(
        verbose_name='Valor anticipo'
    )
    policy_number = models.CharField(
        verbose_name='Número de póliza',
        max_length=50
    )
    scan = models.FileField(
        verbose_name='Archivo digital',
        upload_to=contract_folder,
        blank=True,
        null=True
    )
    archive = models.CharField(
        verbose_name='Ubicación física',
        max_length=10,
        blank=True
    )
    state = models.CharField(
        verbose_name='Estado del contrato',
        default='celebrado',
        choices=STATES_CONTRACT,
        max_length=50
    )

    class Meta:
        verbose_name = 'Contrato',
        verbose_name_plural = 'Contratos'

    def __str__(self):
        return '%d - %d' % (self.contract_number, self.contract_year)

    def get_term(self):
        return (self.end_date - self.init_date).days

    def get_contract_number(self):
        return '%d - %d' % (self.contract_number, self.contract_year)


@python_2_unicode_compatible
class Extension(TimestampUser):
    contract = models.ForeignKey(
        Contract,
        verbose_name='Contrato'
    )
    end_date = models.DateField(
        verbose_name='Fecha de terminación'
    )
    scan = models.FileField(
        verbose_name='Archivo digital',
        blank=True,
        null=True,
        upload_to=extension_folder
    )

    class Meta:
        verbose_name = 'Prórroga',
        verbose_name_plural = 'Prórrogas'

    def __str__(self):
        return 'Prorroga contrato No. %d-%d' % (self.contract.contract_number, self.contract.contract_year)


@python_2_unicode_compatible
class Addition(TimestampUser):
    contract = models.ForeignKey(
        Contract,
        verbose_name='Contrato'
    )
    value = models.PositiveIntegerField(
        verbose_name='Valor adición'
    )
    scan = models.FileField(
        verbose_name='Archivo digital',
        blank=True,
        null=True,
        upload_to=addition_folder
    )

    class Meta:
        verbose_name = 'Adición'
        verbose_name_plural = 'Adiciones'

    def __str__(self):
        return 'Adición contrato No %d-%d' % (self.contract.contract_number, self.contract.contract_year)


@python_2_unicode_compatible
class OtherYes(TimestampUser):
    contract = models.ForeignKey(
        Contract,
        verbose_name='Contrato'
    )
    description = models.TextField(
        verbose_name='Descripción',
        max_length=500
    )
    scan = models.FileField(
        verbose_name='Archivo digital',
        blank=True,
        null=True,
        upload_to=otheryes_folder
    )

    class Meta:
        verbose_name = 'Otro Sí'
        verbose_name_plural = 'Otro Sí'

    def __str__(self):
        return 'Otro Sí contrato No %d-%d' % (self.contract.contract_number, self.contract.contract_year)