# -*- coding: utf-8 -*-
from __future__ import unicode_literals

__author__ = 'gollum23'
__date__ = '3/7/15'

from django import forms
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DetailView, FormView
from django.utils.html import escape

from .models import Contract, Extension, Addition, OtherYes
from .forms import ContractForm, ExtensionForm, AdditionForm, OtherYesForm, FilterForm
from home.mixins import LoginRequiredMixin
from home.forms import TypologyForm, ProjectForm, ResponsiveAreaForm


# Views contracts
class HomeContractView(LoginRequiredMixin, FormView, ListView):
    model = Contract
    template_name = 'contract/index.html'
    form_class = FilterForm
    state = ''
    area = ''
    project = ''
    typology = ''
    year = ''
    amount = 0
    data = {}

    def get_queryset(self):
        self.data = {
            'state': self.state,
            'area': self.area,
            'project': self.project,
            'typology': self.typology,
            'year': self.year,
            'amount': self.amount
        }
        queryset = self.model.objects.all().order_by('-contract_year', '-contract_number')
        if self.request.GET.get('state') is not None and self.request.GET.get('state') != '':
            state = self.request.GET.get('state')
            self.data['state'] = state
            queryset = queryset.filter(state=state)
        if self.request.GET.get('area') is not None and self.request.GET.get('area') != '':
            area = int(self.request.GET.get('area'))
            self.data['area'] = area
            queryset = queryset.filter(responsive_area=area)
        if self.request.GET.get('project') is not None and self.request.GET.get('project') != '':
            project = int(self.request.GET.get('project'))
            self.data['project'] = project
            queryset = queryset.filter(project=project)
        if self.request.GET.get('typology') is not None and self.request.GET.get('typology') != '':
            typology = int(self.request.GET.get('typology'))
            self.data['typology'] = typology
            queryset = queryset.filter(typology=typology)
        if self.request.GET.get('year') is not None and self.request.GET.get('year') != '':
            year = int(self.request.GET.get('year'))
            self.data['year'] = year
            queryset = queryset.filter(contract_year=year)
        if self.request.GET.get('amount') is not None and self.request.GET.get('amount') != '':
            amount = int(self.request.GET.get('amount'))
            self.data['amount'] = amount
            if amount == 1:
                queryset = queryset.filter(total_amount__lte=10000000)
            elif amount == 2:
                queryset = queryset.filter(total_amount__gt=10000000, total_amount__lte=50000000)
            elif amount == 3:
                queryset = queryset.filter(total_amount__gt=50000000)
            else:
                queryset = queryset
        return queryset

    def get_context_data(self, **kwargs):
        context = super(HomeContractView, self).get_context_data(**kwargs)
        context['form'] = self.form_class(initial=self.data)
        return context


class DetailContractView(LoginRequiredMixin, DetailView):
    model = Contract
    template_name = 'contract/detail.html'


class AddContractView(LoginRequiredMixin, CreateView):
    form_class = ContractForm
    template_name = 'contract/add_edit.html'
    success_url = reverse_lazy('contract:home')


class EditContractView(LoginRequiredMixin, UpdateView):
    model = Contract
    form_class = ContractForm
    template_name = 'contract/add_edit.html'
    success_url = reverse_lazy('contract:home')


# Views Extensions
class HomeExtensionView(LoginRequiredMixin, ListView):
    model = Extension
    template_name = 'extension/index.html'

    def get_queryset(self):
        queryset = self.model.objects.all().order_by('-created_at')
        return queryset


class AddExtensionView(LoginRequiredMixin, CreateView):
    form_class = ExtensionForm
    template_name = 'extension/add_edit.html'
    success_url = reverse_lazy('contract:home')

    def get_context_data(self, **kwargs):
        context = super(AddExtensionView, self).get_context_data(**kwargs)
        context['contrato'] = Contract.objects.get(pk=self.kwargs['contract'])
        return context


class EditExtensionView(LoginRequiredMixin, UpdateView):
    model = Extension
    form_class = ExtensionForm
    template_name = 'extension/add_edit.html'
    success_url = reverse_lazy('contract:extension_home')


# Views Additions
class HomeAdditionView(LoginRequiredMixin, ListView):
    model = Addition
    template_name = 'addition/index.html'

    def get_queryset(self):
        queryset = self.model.objects.all().order_by('-created_at')
        return queryset


class AddAdditionView(LoginRequiredMixin, CreateView):
    form_class = AdditionForm
    template_name = 'addition/add_edit.html'
    success_url = reverse_lazy('contract:home')

    def get_context_data(self, **kwargs):
        context = super(AddAdditionView, self).get_context_data(**kwargs)
        context['contrato'] = Contract.objects.get(pk=self.kwargs['contract'])
        return context


class EditAdditionView(LoginRequiredMixin, UpdateView):
    model = Addition
    form_class = AdditionForm
    template_name = 'addition/add_edit.html'
    success_url = reverse_lazy('contract:addition_home')


# Views Otheryes
class HomeOtherYesView(LoginRequiredMixin, ListView):
    model = OtherYes
    template_name = 'otheryes/index.html'

    def get_queryset(self):
        queryset = self.model.objects.all().order_by('-created_at')
        return queryset


class AddOtherYesView(LoginRequiredMixin, CreateView):
    form_class = OtherYesForm
    template_name = 'otheryes/add_edit.html'
    success_url = reverse_lazy('contract:home')

    def get_context_data(self, **kwargs):
        context = super(AddOtherYesView, self).get_context_data(**kwargs)
        context['contrato'] = Contract.objects.get(pk=self.kwargs['contract'])
        return context


class EditOtherYesView(LoginRequiredMixin, UpdateView):
    model = OtherYes
    form_class = OtherYesForm
    template_name = 'otheryes/add_edit.html'
    success_url = reverse_lazy('contract:otheryes_home')


# Inlines
def handle_pop_up_add(request, add_form, field):
    form = add_form(request.POST or None)
    if form.is_valid():
        try:
            new_object = form.save()
        except forms.ValidationError:
            new_object = None
        if new_object:
            return HttpResponse(
                '<script type="text/javascript">opener.dismissAddAnotherPopup(window, "%s", "%s");</script>' %
                (escape(new_object._get_pk_val()), escape(new_object)))
    ctx = {
        'form': form,
        'field': field
    }
    # if field == 'contractor':
    #     template = 'popaddcontrator.html'
    template = 'popaddtypology.html'
    if field == 'typology':
        template = 'popaddtypology.html'
    if field == 'project':
        template = 'popaddproject.html'
    if field == 'responsive_area':
        template = 'popaddarea.html'
    return render(request, template, ctx)


@login_required(login_url='/')
def add_typology(request):
    return handle_pop_up_add(request, TypologyForm, 'typology')


@login_required(login_url='/')
def add_project(request):
    return handle_pop_up_add(request, ProjectForm, 'project')


@login_required(login_url='/')
def add_responsive_area(request):
    return handle_pop_up_add(request, ResponsiveAreaForm, 'responsive_area')
