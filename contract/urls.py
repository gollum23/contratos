# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'gollum23'
__date__ = '3/7/15'

from django.conf.urls import patterns, url
from .views import HomeContractView, AddContractView, HomeExtensionView, AddExtensionView, EditExtensionView, \
    HomeAdditionView, AddAdditionView, EditAdditionView, HomeOtherYesView, AddOtherYesView, EditOtherYesView, \
    EditContractView, DetailContractView, add_typology, add_project, add_responsive_area

urlpatterns = patterns(
    'contract.views',
    url(r'^$', HomeContractView.as_view(), name='home'),
    url(r'^detail/(?P<pk>[\d]+)/$', DetailContractView.as_view(), name='detail'),
    url(r'^add/$', AddContractView.as_view(), name='add'),
    url(r'^edit/(?P<pk>[\d]+)/$', EditContractView.as_view(), name='edit'),
    url(r'^prorrogas/$', HomeExtensionView.as_view(), name='extension_home'),
    url(r'^prorrogas/add/(?P<contract>\d+)/$', AddExtensionView.as_view(), name='extension_add_inline'),
    url(r'^prorrogas/edit/(?P<pk>\d+)/$', EditExtensionView.as_view(), name='extension_edit'),
    url(r'^adiciones/$', HomeAdditionView.as_view(), name='addition_home'),
    url(r'^adiciones/add/(?P<contract>\d+)/$', AddAdditionView.as_view(), name='addition_add_inline'),
    url(r'^adiciones/edit/(?P<pk>\d+)/$', EditAdditionView.as_view(), name='addition_edit'),
    url(r'^otrosi/$', HomeOtherYesView.as_view(), name='otheryes_home'),
    url(r'^otrosi/add/(?P<contract>\d+)/$', AddOtherYesView.as_view(), name='otheryes_add_inline'),
    url(r'^otrosi/edit/(?P<pk>\d+)/$', EditOtherYesView.as_view(), name='otheryes_edit'),
    url(r'^add/typology/$', add_typology, name='add_typology_inline'),
    url(r'^add/project/$', add_project, name='add_project_inline'),
    url(r'^add/responsive_area/$', add_responsive_area, name='add_area_inline'),
)

