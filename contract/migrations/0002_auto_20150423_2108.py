# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contract',
            name='archive',
            field=models.CharField(max_length=10, verbose_name='Ubicaci\xf3n f\xedsica', blank=True),
        ),
    ]
