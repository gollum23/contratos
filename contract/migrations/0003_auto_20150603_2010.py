# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import contract.models


class Migration(migrations.Migration):

    dependencies = [
        ('contract', '0002_auto_20150423_2108'),
    ]

    operations = [
        migrations.AddField(
            model_name='addition',
            name='scan',
            field=models.FileField(upload_to=contract.models.addition_folder, null=True, verbose_name='Archivo digital', blank=True),
        ),
        migrations.AddField(
            model_name='contract',
            name='state',
            field=models.CharField(default='celebrado', max_length=50, verbose_name='Estado del contrato', choices=[('celebrado', 'Celebrado'), ('ejecucion', 'En ejecuci\xf3n'), ('terminado', 'Terminado')]),
        ),
        migrations.AddField(
            model_name='extension',
            name='scan',
            field=models.FileField(upload_to=contract.models.extension_folder, null=True, verbose_name='Archivo digital', blank=True),
        ),
        migrations.AddField(
            model_name='otheryes',
            name='scan',
            field=models.FileField(upload_to=contract.models.otheryes_folder, null=True, verbose_name='Archivo digital', blank=True),
        ),
    ]
