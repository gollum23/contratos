# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import contract.models


class Migration(migrations.Migration):

    dependencies = [
        ('contractor', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Addition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('value', models.PositiveIntegerField(verbose_name='Monto adici\xf3n')),
            ],
            options={
                'verbose_name': 'Adici\xf3n',
                'verbose_name_plural': 'Adiciones',
            },
        ),
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('contract_number', models.PositiveIntegerField(verbose_name='N\xfamero de contrato')),
                ('contract_year', models.PositiveIntegerField(default=2015)),
                ('contract_object', models.TextField(verbose_name='Objeto del contrato')),
                ('subscription_date', models.DateField(verbose_name='Fecha de suscripci\xf3n')),
                ('init_date', models.DateField(verbose_name='Fecha de inicio')),
                ('end_date', models.DateField(verbose_name='Fecha de terminaci\xf3n')),
                ('total_amount', models.PositiveIntegerField(verbose_name='Monto total')),
                ('advance_amount', models.PositiveIntegerField(verbose_name='Monto avance')),
                ('policy_number', models.CharField(max_length=50, verbose_name='N\xfamero de p\xf3liza')),
                ('scan', models.FileField(upload_to=contract.models.contract_folder, null=True, verbose_name='Archivo digital', blank=True)),
                ('archive', models.CharField(max_length=10, verbose_name='Ubicaci\xf3n f\xedsica')),
                ('contractor', models.ForeignKey(verbose_name='Contratista', to='contractor.Contractor')),
                ('project', models.ForeignKey(verbose_name='Proyecto', to='home.Project')),
                ('responsive_area', models.ForeignKey(verbose_name='\xc1rea responsable', to='home.ResponsiveArea')),
                ('typology', models.ForeignKey(verbose_name='Tipolog\xeda', to='home.Typology')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': ('Contrato',),
                'verbose_name_plural': 'Contratos',
            },
        ),
        migrations.CreateModel(
            name='ContractState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_state', models.CharField(max_length=100, verbose_name='Estado de Contrato')),
            ],
            options={
                'verbose_name': 'Estado de contratos',
                'verbose_name_plural': 'Estados de contratos',
            },
        ),
        migrations.CreateModel(
            name='Extension',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('end_date', models.DateField(verbose_name='Nueva fecha')),
                ('contract', models.ForeignKey(verbose_name='Contrato', to='contract.Contract')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': ('Prorroga',),
                'verbose_name_plural': 'Prorrogas',
            },
        ),
        migrations.CreateModel(
            name='OtherYes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('description', models.TextField(max_length=500, verbose_name='Descripci\xf3n')),
                ('contract', models.ForeignKey(verbose_name='Contrato', to='contract.Contract')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Otro S\xed',
                'verbose_name_plural': 'Otro S\xed',
            },
        ),
        migrations.AddField(
            model_name='addition',
            name='contract',
            field=models.ForeignKey(verbose_name='Contrato', to='contract.Contract'),
        ),
        migrations.AddField(
            model_name='addition',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
