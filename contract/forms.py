# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'gollum23'
__date__ = '3/7/15'
from datetime import datetime
from django import forms
from django.template.loader import render_to_string

from .models import Contract, Extension, Addition, OtherYes, STATES_CONTRACT
from home.models import ResponsiveArea, Project, Typology
from contractor.models import Contractor


class SelectWithPop(forms.Select):

    def render(self, name, *args, **kwargs):
        html = super(SelectWithPop, self).render(name, *args, **kwargs)
        popupplus = render_to_string("popupplus.html", {'field': name})

        return html + popupplus


class MultipleSelectWithPop(forms.Select):

    def render(self, name, *args, **kwargs):
        html = super(MultipleSelectWithPop, self).render(name, *args, **kwargs)
        popupplus = render_to_string("popupplus.html", {'field': name})

        return html + popupplus


class ContractForm(forms.ModelForm):
    contract_number = forms.IntegerField(
        widget=forms.TextInput(
            attrs={
                'class': 'input--d4'
            }
        ),
        label='Número de Contrato'

    )
    contract_year = forms.IntegerField(
        widget=forms.TextInput(
            attrs={
                'class': 'input--d4'
            }
        ),
        initial=datetime.now().year
    )
    contract_object = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': 'input__text input--full-width'
            }
        ),
        label='Objeto del contrato'

    )
    # contractor = forms.ModelChoiceField(
    #     Contractor.objects,
    #     widget=SelectWithPop,
    #     label='Contratista'
    # )
    typology = forms.ModelChoiceField(
        Typology.objects,
        widget=SelectWithPop,
        label='Tipología'
    )
    project = forms.ModelChoiceField(
        Project.objects,
        widget=SelectWithPop,
        label='Proyecto'
    )
    responsive_area = forms.ModelChoiceField(
        ResponsiveArea.objects,
        widget=SelectWithPop,
        label='Area'
    )

    class Meta:
        model = Contract
        fields = '__all__'


class ExtensionForm(forms.ModelForm):

    class Meta:
        model = Extension
        fields = '__all__'


class AdditionForm(forms.ModelForm):

    class Meta:
        model = Addition
        fields = '__all__'


class OtherYesForm(forms.ModelForm):

    class Meta:
        model = OtherYes
        fields = '__all__'


class EmptyChoiceField(forms.ChoiceField):
    def __init__(self, choices=(), empty_label=None, required=True, widget=None, label=None,
                 initial=None, help_text=None, *args, **kwargs):

        # prepend an empty label if it exists (and field is not required!)
        if not required and empty_label is not None:
            choices = tuple([(u'', empty_label)] + list(choices))

        super(EmptyChoiceField, self).__init__(choices=choices, required=required, widget=widget, label=label,
                                               initial=initial, help_text=help_text, *args, **kwargs)


AMOUNT_RANGE_CHOICES = (
    (1, '< 10.000.000'),
    (2, '10.000.001 - 50.000.000'),
    (3, '> 50.000.001'),
)



class FilterForm(forms.Form):
    state = EmptyChoiceField(
        choices=STATES_CONTRACT,
        label='Estado',
        empty_label='--------',
        required=False,
        initial=0
    )
    area = EmptyChoiceField(
        label='Área',
        required=False
    )
    project = EmptyChoiceField(
        label='Proyecto',
        required=False
    )
    typology = EmptyChoiceField(
        label='Tipo',
        required=False
    )
    year = EmptyChoiceField(
        label='Año',
        required=False
    )
    amount = EmptyChoiceField(
        choices=AMOUNT_RANGE_CHOICES,
        label='Monto',
        empty_label='--------',
        required=False,
        initial=0
    )

    def __init__(self, *args, **kwargs):
        super(FilterForm, self).__init__(*args, **kwargs)
        empty_label = [('', '------------')]
        years = empty_label + list(Contract.objects.values_list('contract_year', 'contract_year').distinct().order_by('-contract_year'))
        self.fields['year'].choices = years
        tipologies = empty_label + list(Typology.objects.values_list('id', 'name_typology').order_by('name_typology'))
        self.fields['typology'].choices = tipologies
        projects = empty_label + list(Project.objects.values_list('id', 'name_resource').order_by('name_resource'))
        self.fields['project'].choices = projects
        areas = empty_label + list(ResponsiveArea.objects.values_list('id', 'name_area').order_by('name_area'))
        self.fields['area'].choices = areas
