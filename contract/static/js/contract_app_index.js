/**
 * Created by gollum23 on 06/06/15.
 */

(function() {
    $('#reset_filter_form' ).on('click', resetFilterForm);

    function resetFilterForm(e) {
        e.preventDefault();
        $ ('.form__filter' ).find('select' ).val('');
        window.location.replace('/contratos/');
    }
})();