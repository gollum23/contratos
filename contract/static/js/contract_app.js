/**
 * Created by gollum23 on 23/04/15.
 */

(function() {
    var $ = window.jQuery;
    // Instance chosen for contractor, responsive area and project selects
    $('#id_contractor' ).chosen();
    $('#id_responsive_area' ).chosen();
    $('#id_project' ).chosen();
    // Include Date picker
    $('#id_subscription_date, #id_init_date, #id_end_date').pickadate({
        format: 'dd/mm/yyyy',
        selectYears: true,
        selectMonth: true
    });


    $(window ).on('beforeunload', function() {
        return "Los cambios no guardados se perderan.";
    });

    $('#formContract').on('submit', function() {
        $(window ).off('beforeunload');
    })
})();