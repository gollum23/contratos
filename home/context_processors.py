# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '3/14/15'
from django.core.urlresolvers import reverse


def menu(request):
    items_menu = {
        'menu': [
            {'name': 'Dashboard', 'url': reverse('home:home'), 'icon': 'dashboard'},
            {'name': 'Contratos', 'url': reverse('contract:home'), 'icon': 'file-text'},
            # {'name': 'Prórrogas', 'url': reverse('contract:extension_home'), 'icon': 'calendar'},
            # {'name': 'Adiciones', 'url': reverse('contract:addition_home'), 'icon': 'dollar'},
            # {'name': 'Otrosi', 'url': reverse('contract:otheryes_home'), 'icon': 'file-text-o'},
            {'name': 'Contratistas', 'url': reverse('contractor:home'), 'icon': 'users'},
            # {'name': 'Pagos', 'url': reverse('payment:home'), 'icon': 'money'},
            # {'name': 'Reportes', 'url': reverse('home:home'), 'icon': 'newspaper-o'},
        ]
    }
    for item in items_menu['menu']:
        if request.path == item['url']:
            item['active'] = True

    return items_menu