# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '3/7/15'

from django.contrib.auth.forms import AuthenticationForm
from django import forms

from .models import Typology, Project, ResponsiveArea


class CustomAuthenticationForm(AuthenticationForm):
    """
    Custom form created from AuthenticationForm
    """
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Usuario', 'class': 'form-login__input--user'}
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'placeholder': 'Contraseña', 'class': 'form-login__input--pass'}
        )
    )


class TypologyForm(forms.ModelForm):

    class Meta:
        model = Typology
        fields = '__all__'


class ProjectForm(forms.ModelForm):

    class Meta:
        model = Project
        fields = '__all__'


class ResponsiveAreaForm(forms.ModelForm):

    class Meta:
        model = ResponsiveArea
        fields = '__all__'
