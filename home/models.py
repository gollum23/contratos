# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'gollum23'
__date__ = '3/7/15'
from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible


class TimestampUser(models.Model):
    """
    Abstract class with timestamps and user fields
    """
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True
    )
    user = models.ForeignKey(
        User
    )

    class Meta:
        abstract = True


@python_2_unicode_compatible
class Project(models.Model):
    name_resource = models.CharField(
        verbose_name='Proyecto',
        max_length=100
    )

    class Meta:
        verbose_name = 'Proyecto'
        verbose_name_plural = 'Proyectos'
        ordering = ['name_resource']

    def __str__(self):
        return '%s' % self.name_resource


@python_2_unicode_compatible
class ResponsiveArea(models.Model):
    name_area = models.CharField(
        verbose_name='Área responsable',
        max_length=100
    )
    responsive = models.CharField(
        verbose_name='Persona responsable',
        max_length=150
    )
    email = models.EmailField(
        verbose_name='Correo electrónico',
    )

    class Meta:
        verbose_name = 'Área responsable'
        verbose_name_plural = 'Áreas responsables'
        ordering = ['name_area']

    def __str__(self):
        return '%s' % self.name_area


@python_2_unicode_compatible
class Typology(models.Model):
    name_typology = models.CharField(
        verbose_name='Tipología',
        max_length=100
    )

    class Meta:
        verbose_name = 'Tipología',
        verbose_name_plural = 'Tipologías'
        ordering = ['name_typology']

    def __str__(self):
        return '%s' % self.name_typology