# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_resource', models.CharField(max_length=100, verbose_name='Proyecto')),
            ],
            options={
                'verbose_name': 'Proyecto',
                'verbose_name_plural': 'Proyectos',
            },
        ),
        migrations.CreateModel(
            name='ResponsiveArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_area', models.CharField(max_length=100, verbose_name='\xc1rea responsable')),
                ('responsive', models.CharField(max_length=150, verbose_name='Persona responsable')),
                ('email', models.EmailField(max_length=254, verbose_name='Correo electr\xf3nico')),
            ],
            options={
                'verbose_name': '\xc1rea responsable',
                'verbose_name_plural': '\xc1reas responsables',
            },
        ),
        migrations.CreateModel(
            name='Typology',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_typology', models.CharField(max_length=100, verbose_name='Tipolog\xeda')),
            ],
            options={
                'verbose_name': ('Tipolog\xeda',),
                'verbose_name_plural': 'Tipolog\xedas',
            },
        ),
    ]
