# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='project',
            options={'ordering': ['name_resource'], 'verbose_name': 'Proyecto', 'verbose_name_plural': 'Proyectos'},
        ),
        migrations.AlterModelOptions(
            name='responsivearea',
            options={'ordering': ['name_area'], 'verbose_name': '\xc1rea responsable', 'verbose_name_plural': '\xc1reas responsables'},
        ),
        migrations.AlterModelOptions(
            name='typology',
            options={'ordering': ['name_typology'], 'verbose_name': ('Tipolog\xeda',), 'verbose_name_plural': 'Tipolog\xedas'},
        ),
    ]
