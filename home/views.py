# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '3/7/15'
from datetime import date, timedelta
from django.contrib.auth import login, logout
from django.shortcuts import redirect, render

from .forms import CustomAuthenticationForm
from contract.models import Contract


def home_view(request):
    if request.user.is_authenticated():
        # Last contract saved
        last_contract = Contract.objects.last()
        # Contracts without scan
        contract_without_scan = Contract.objects.filter(scan='').order_by('-contract_year', '-contract_number')
        # Contracts expires less 45 days
        month = date.today() + timedelta(days=45)  # Today plus 45 days
        contract_expires = Contract.objects.filter(end_date__lte=month, end_date__gte=date.today())
        ctx = {
            'last_contract': last_contract,
            'contract_without_scan': contract_without_scan,
            'contract_expires': contract_expires
        }
        return render(request, 'home__index.html', ctx)
    else:
        form = CustomAuthenticationForm(request, request.POST or None)
        if form.is_valid():
            if form.get_user() is not None:
                login(request, form.get_user())
                return redirect(request.path)
        ctx = {
            'form': form
        }
        return render(request, 'home__login.html', ctx)


def logout_view(request):
    logout(request)
    return redirect('/')