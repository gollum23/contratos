/**
 * Created by gollum23 on 3/8/15.
 */
(function () {
    'use strict';

    $('.user').on('click', toogleMenu);

    function toogleMenu(e) {
        var menu = $('.user__menu');
        menu.slideToggle().removeClass('is_hidden');
    }

})();