# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'gollum23'
__date__ = '3/7/15'

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from contract.models import Contract
from home.models import TimestampUser


@python_2_unicode_compatible
class Bank(models.Model):
    name_bank = models.CharField(
        verbose_name='Nombre del banco',
        max_length=100
    )

    class Meta:
        verbose_name = 'Banco'
        verbose_name_plural = 'Bancos'

    def __str__(self):
        return '%s' % self.name_bank


@python_2_unicode_compatible
class PaymentType(models.Model):
    name_type = models.CharField(
        verbose_name='Forma de pago',
        max_length=100
    )

    class Meta:
        verbose_name = 'Método de pago'
        verbose_name_plural = 'Métodos de pago'

    def __str__(self):
        return '%s' % self.name_type


@python_2_unicode_compatible
class Payment(TimestampUser):
    contract = models.ForeignKey(
        Contract,
        verbose_name='Contrato No.'
    )
    payment_amount = models.PositiveIntegerField(
        verbose_name='Monto pagado'
    )
    payment_date = models.DateField(
        verbose_name='Fecha de pago'
    )
    payment_bank = models.ForeignKey(
        Bank,
        verbose_name='Banco'
    )
    payment_type = models.ForeignKey(
        PaymentType,
        verbose_name='Forma de pago'
    )
    payment_number = models.CharField(
        verbose_name='Número de cheque o transferencia',
        max_length=50
    )

    class Meta:
        verbose_name = 'Pago'
        verbose_name_plural = 'Pagos'

    def __str__(self):
        return '%d - %d - %d - %s' % (self.id, self.contract.contract_number, self.payment_amount, self.payment_date)