# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '3/7/15'

from django.conf.urls import patterns, url

from .views import HomePaymentView

urlpatterns = patterns(
    'payment.views',
    url(r'^$', HomePaymentView.as_view(), name='home'),
    # url(r'^edit/(?P<id>[\d]+)/$', 'edit_contract_view', name='edit'),
)