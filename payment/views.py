# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '3/7/15'

from django.views.generic import ListView

from home.mixins import LoginRequiredMixin
from .models import Payment


class HomePaymentView(LoginRequiredMixin, ListView):
    model = Payment
    template_name = 'payment__home.html'

    def get_queryset(self):
        queryset = self.model.objects.all().order_by('-payment_date')
        return queryset