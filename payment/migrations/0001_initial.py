# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contract', '0003_auto_20150603_2010'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_bank', models.CharField(max_length=100, verbose_name='Nombre del banco')),
            ],
            options={
                'verbose_name': 'Banco',
                'verbose_name_plural': 'Bancos',
            },
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('payment_amount', models.PositiveIntegerField(verbose_name='Monto pagado')),
                ('payment_date', models.DateField(verbose_name='Fecha de pago')),
                ('payment_number', models.CharField(max_length=50, verbose_name='N\xfamero de cheque o transferencia')),
                ('contract', models.ForeignKey(verbose_name='Contrato No.', to='contract.Contract')),
                ('payment_bank', models.ForeignKey(verbose_name='Banco', to='payment.Bank')),
            ],
            options={
                'verbose_name': 'Pago',
                'verbose_name_plural': 'Pagos',
            },
        ),
        migrations.CreateModel(
            name='PaymentType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_type', models.CharField(max_length=100, verbose_name='Forma de pago')),
            ],
            options={
                'verbose_name': 'M\xe9todo de pago',
                'verbose_name_plural': 'M\xe9todos de pago',
            },
        ),
        migrations.AddField(
            model_name='payment',
            name='payment_type',
            field=models.ForeignKey(verbose_name='Forma de pago', to='payment.PaymentType'),
        ),
        migrations.AddField(
            model_name='payment',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
